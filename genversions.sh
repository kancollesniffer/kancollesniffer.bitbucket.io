#!/bin/bash

cp -p ../src/LogViewer/proxy.pac .
cp -p ../src/Data/EnemySlot.csv .
cp -p ../src/Data/minasw.csv .

echo "`cat version` KancolleSniffer" > versions.txt
sha256sum proxy.pac     >> versions.txt
sha256sum EnemySlot.csv >> versions.txt
sha256sum minasw.csv    >> versions.txt
