function FindProxyForURL(url, host) {
    if (host == "w01y.kancolle-server.com" ||
        host == "w02k.kancolle-server.com" ||
        host == "w03s.kancolle-server.com" ||
        host == "w04m.kancolle-server.com" ||
        host == "w05o.kancolle-server.com" ||
        host == "w06t.kancolle-server.com" ||
        host == "w07l.kancolle-server.com" ||
        host == "w08r.kancolle-server.com" ||
        host == "w09s.kancolle-server.com" ||
        host == "w10b.kancolle-server.com" ||
        host == "w11t.kancolle-server.com" ||
        host == "w12p.kancolle-server.com" ||
        host == "w13b.kancolle-server.com" ||
        host == "w14h.kancolle-server.com" ||
        host == "w15p.kancolle-server.com" ||
        host == "w16s.kancolle-server.com" ||
        host == "w17k.kancolle-server.com" ||
        host == "w18i.kancolle-server.com" ||
        host == "w19s.kancolle-server.com" ||
        host == "w20h.kancolle-server.com") {
        return "PROXY 127.0.0.1:8080";
    }
    else {
        return "DIRECT";
    }
}
